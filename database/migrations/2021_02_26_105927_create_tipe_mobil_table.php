<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipeMobilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipe_mobil', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->foreignId('tipe_kendaraan_id')->constrained('tipe_kendaraan');
            $table->foreignId('merek_mobil_id')->constrained('merek_mobil');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipe_mobil');
    }
}
