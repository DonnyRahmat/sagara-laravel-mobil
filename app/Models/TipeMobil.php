<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Observers\ProvincesObserver as Observer;
use Illuminate\Support\Facades\Schema;

class TipeMobil extends Resources {

	protected $table = 'tipe_mobil';

    protected $rules = array(
        'name' => 'required|string',
        'merek_mobil_id' => 'required|integer',
        'tipe_kendaraan_id' => 'required|integer',
    );

    protected $forms = array(
        [
            [
                'class' => 'col-4',
                'field' => 'name'
            ],
            [
                'class' => 'col-4',
                'field' => 'tipe_kendaraan_id'
            ],
            [
                'class' => 'col-4',
                'field' => 'merek_mobil_id'
            ]
        ],
    );

    protected $structures = array(
        "id" => [
            'name' => 'id',
            'default' => null,
            'label' => 'ID',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => true,
            'required' => true,
            'type' => 'integer',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],

        "name" => [
            'name' => 'name',
            'default' => null,
            'label' => 'Tipe Mobil',
            'display' => true,
            'validation' => [
                'create' => 'required|string',
                'update' => 'required|string',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'text',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Tipe Mobil',
        ],

        "tipe_kendaraan_id" => [
            'name' => 'tipe_kendaraan_id',
            'default' => null,
            'label' => 'Tipe Kendaraan',
            'display' => true,
            'validation' => [
                'create' => 'required|integer',
                'update' => 'required|integer',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'reference',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Tipe Kendaraan',
            // Options reference
            'reference' => "TipeKendaraan", // Select2 API endpoint => /api/v1/countries
            'relationship' => 'tipe_kendaraan', // relationship request datatable
            'option' => [
                'value' => 'id',
                'label' => 'name'
            ]
        ],
        "merek_mobil_id" => [
            'name' => 'merek_mobil_id',
            'default' => null,
            'label' => 'Merek Mobil',
            'display' => true,
            'validation' => [
                'create' => 'required|integer',
                'update' => 'required|integer',
                'delete' => null,
            ],
            'primary' => false,
            'required' => true,
            'type' => 'reference',
            'validated' => true,
            'nullable' => false,
            'note' => null,
            'placeholder' => 'Merek Mobil',
            // Options reference
            'reference' => "MerekMobil", // Select2 API endpoint => /api/v1/countries
            'relationship' => 'merek_mobil', // relationship request datatable
            'option' => [
                'value' => 'id',
                'label' => 'name'
            ]
        ],


        "created_at" => [
            'name' => 'created_at',
            'default' => null,
            'label' => 'Created At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'required' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "updated_at" => [
            'name' => 'updated_at',
            'default' => null,
            'label' => 'Updated At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'required' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ],
        "deleted_at" => [
            'name' => 'deleted_at',
            'default' => null,
            'label' => 'Deleted At',
            'display' => false,
            'validation' => [
                'create' => null,
                'update' => null,
                'delete' => null,
            ],
            'primary' => false,
            'required' => false,
            'type' => 'datetime',
            'validated' => false,
            'nullable' => false,
            'note' => null
        ]
    );

    protected $searchable = array('name', 'merek_mobil_id', 'tipe_kendaraan_id');
    protected $casts = [
        'country' => 'array',
    ];

    //  OBSERVER
    protected static function boot() {
        parent::boot();
        static::observe(Observer::class);
    }

    public function tipe_kendaraan() {
        return $this->belongsTo('App\Models\TipeKendaraan', 'tipe_kendaraan_id', 'id')->withTrashed();
    }

    public function merek_mobil() {
        return $this->belongsTo('App\Models\MerekMobil', 'merek_mobil_id', 'id')->withTrashed();
    }

}
